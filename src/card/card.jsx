import { Component } from "react";
import styles from './card.module.css';

export class Card extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (<div className={styles.divcard}>
            <img src={this.props.image}/>
            <div className={styles.title}>{this.props.title}</div>
            <div className={styles.price}>{this.props.price + ' $'}</div>
            <div className={styles.description}>{this.props.desc}</div>
            <button>Purchase</button>
            <div className={`${styles.rating} ${this.getRatingClassName(this.props.rating)}`}>{this.props.rating}</div>
        </div>);
    }

    getRatingClassName(rate) {
        if (rate === 5) {
            return "";
        } else if (rate < 5 && rate >= 4) {
            return styles.rating_good;
        } else if (rate < 4 && rate >= 3) {
            return styles.rating_middle;
        } else if (rate < 3 && rate >=2) {
            return styles.rating_bad;
        } else {
            return styles.rating_verybad;
        }
    }
}