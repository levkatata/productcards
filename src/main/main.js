import { Component } from "react";
import { Card } from "../card/card";
import './main.module.css';

export class Main extends Component {
    constructor(props) {
      super(props);
      this.state = {
        error: null,
        isLoaded: false,
        items: []
      };
    }
  
    componentDidMount() {
      fetch("https://fakestoreapi.com/products")
        .then(res => res.json())
        .then(
          (result) => {
            console.log(result);
            this.setState({
              isLoaded: true,
              items: result
            });
          },
          // Note: it's important to handle errors here
          // instead of a catch() block so that we don't swallow
          // exceptions from actual bugs in components.
          (error) => {
            this.setState({
              isLoaded: true,
              error
            });
          }
        )
    }
  
    render() {
      const { error, isLoaded, items } = this.state;
      console.dir(items);
      if (error) {
        return <div>Error: {error.message}</div>;
      } else if (!isLoaded) {
        return <div>Loading...</div>;
      } else {
        return (
            <main>
                <section>
                    {items.map((i) => {
                        return <Card 
                            title={i.title} 
                            image={i.image} 
                            price={i.price} 
                            desc={i.description}
                            rating={i.rating.rate}/>
                    })}
                </section>
            </main>
        );
      }
    }
  }